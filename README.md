# py-data-splitter
A useful python script to split a large data file to smaller data files, which the number of data/lines in each sub folder is defined by the user. Currently, it only splits text (data) files that is pre-formatted.


##How to use?
- Download dataSplit.py and run it as a normal python script. 
- Select the data file to be split.
- A directory will be created with the datafile name & length within the selected file directory. If there is an exisiting directory without any contents in it, the folder will be used. Else, the scipt will end.
- The console will list down how many parts the file will be broken into, with the number of data/lines beside the sub files name.


##About
This python script is written in Python 3.5 and is not tested in other versions of Python.


##Changelog
v1.0.0:
- Initial release

v1.0.1:
- Fixed bug where output files contain duplicate split section
- Missing colon for the 'for' loop
- Fixed exit error (should have used 'exit()' instead of 'break')
- Change exit delay from 3s to 0s

