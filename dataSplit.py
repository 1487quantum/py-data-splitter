#This python file splits text file into multiple smaller text files

import tkinter as tk        #GUI stuff
import os
import ntpath
import time
import sys

from tkinter import filedialog

exitDelay = 0 #Pause for 0s before exit

l = ''
for i in range(0,40):
    l+='-'
print(l)
print('------- Data splitter v1.01 -----------')
print(l)

while True:
    print('Select data file...')
    #Open Selection Dialog
    root = tk.Tk()
    root.withdraw()
    file_path = filedialog.askopenfilename(title = "Select data file",filetypes = (("Text files","*.txt"),("all files","*.*")))

    if file_path=='':
        print('Selection cancelled. Quitting...')
        time.sleep(exitDelay)
        sys.exit()

    #Extract filename
    ntpath.basename("a/b/c")
    headDir, fName = ntpath.split(file_path)

    #Data file path
    file = open(file_path, "r")     #Open data file

    fName, ext = ntpath.splitext(fName)     #Extract filename w/o extension
    print('\n'+fName + ' selected.')         #Print selected item

    #Get number of lines of data the file has
    nLines = file.readlines()
    dLength = len(nLines)
    print('Data Length: '+str(dLength))

    dir = os.path.dirname(file_path)

    #Create new directory for split data
    if not os.path.exists(dir+'/'+fName+'_'+str(dLength)):
        print(fName+" does not exist. Creating folder...\n");
        os.makedirs(dir+'/'+fName+'_'+str(dLength))
    else:
        #Check if directory is empty
        for root, dirs, files in os.walk(dir+'/'+fName+'_'+str(dLength)):
            dirlen=len(dirs)
            filelen=len(files)
            if filelen == 0 and dirlen == 0:
                print(fName+" folder is empty. Using folder...\n");
            else:
                #If directory not empty, exit program
                print(fName+" folder contains item. Quiting...");
                time.sleep(exitDelay)   #Pause for 1s
                sys.exit()
                pass

    curDir = dir+'/'+fName      #Current directory
    os.chdir(curDir+'_'+str(dLength))   #Change to working directory
    #print(os.getcwd())

    #Enter lines per subfile
    lines_per_file = 2000       #Default
    while True:
        try:
            lines_per_file = int(input('Enter the number of data/lines per file: '))
        except ValueError:
           print("That's not an int!")
           continue
        else:
            if lines_per_file>dLength:
                print('Lines per file cannot exceed data length!\n')
            else:
                print('Lines per file: %d\n'%lines_per_file)
                break

    #Start timing (For debugging)
    startTime = time.time()

    #Main splitting function
    numFiles = int(dLength/lines_per_file)       #Number of files to split into
    if dLength % lines_per_file!=0:
        print('Splitting into %d subfiles:'%(numFiles+1))
    else:
        print('Splitting into %d subfiles:'%numFiles)
    #print(numFiles)

    fCount = 0      #Num of files Counter, used for naming the smaller sub files
    for fNum in range(0,numFiles):
        subFile_name = fName+ '_' + str(fNum)+ '_' + str(lines_per_file)+".txt"
        sF = open(subFile_name,'w')
        for i in range(fCount*lines_per_file,(fCount+1)*lines_per_file):
            sF.write(''.join(nLines[i]))
        sF.close()
        print(subFile_name+': %d'%(i+1-fCount*lines_per_file))
        fCount+=1

    if dLength % lines_per_file!=0:         #Where data length is NOT in multiples of lines_per_file
        dRemain = dLength - numFiles*lines_per_file
        subFile_name = fName+ '_' + str(fCount)+ '_' + str(dRemain)+".txt"
        sF = open(subFile_name,'w')
        for i in range(0,dRemain):
            sF.write(''.join(nLines[i+numFiles*lines_per_file]))
        sF.close()
        print(subFile_name+': %d'%(i+1))

    print('\nSplitting Done! Completed in %.2fs.'%(time.time() - startTime))

    # raw_input returns the empty string for "enter"
    yes = set(['yes','y', ''])
    no = set(['no','n'])

    while True:
        choice = input('\nContinue splitting(Y/N)? ').lower()
        if choice in yes:
            break
            break
        elif choice in no:
            print('Quiting...')
            time.sleep(exitDelay)
            sys.exit()
        else:
           print("Please respond with '(Y)es' or '(N)o'")
